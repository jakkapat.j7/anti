# ทอนเหรียญ 38 บาท

def change(value):
    coin = [10, 5, 4, 2, 1]
    coin_change = dict()

    for i in coin:
        if value >= i:
            coin_change[i] = value // i
            value = value % i

    return coin_change



if __name__ == "__main__":
    print(change(38))