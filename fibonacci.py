import timeit

def recursive_solution(fibonacci):
    if fibonacci > 1:
        return recursive_solution(fibonacci - 1) + recursive_solution(fibonacci - 2)
    else:
        return fibonacci

def bottomup_solution(fibonacci):
    f0 = 0
    f1 = 1
    f2 = 1

    for i in range(fibonacci - 1):
        f2 = f0 + f1
        f0 = f1
        f1 = f2
    
    return f2

print("Recursive: ", recursive_solution(10), timeit.timeit("recursive_solution(10)", globals=globals()))
print("Bottom-up: ", bottomup_solution(10), timeit.timeit("bottomup_solution(10)", globals=globals()))
